package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    double getBookPrice(String isbn) {
        double fahrenheit = Double.parseDouble(isbn);
        return (Double) ((fahrenheit-32) * (5/9));
    }
}
